const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();
router.get("/", (req,res,next) =>{
    try {
        const users = UserService.getAll();
        res.data = users;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get("/:id", (req, res, next) =>{
    try {
        const id = {
            id: req.params.id
        };
        const user = UserService.getOne(id);
        res.data = user;
    } catch (err) {
        res.err = err;
    } finally{
        next();
    }
},  responseMiddleware);

router.post("/", createUserValid, (req, res, next) => {
    if(!(res.err && res.err.error)) {
        UserService.create(res.data);
    }
    next();
}, responseMiddleware);

router.put("/:id", (req, res, next) => {
        const id = {id: req.params.id};
        const dataToUpdate = req.query;
        const model = UserService.getOne(id);
        Object.assign(model, dataToUpdate);
        res.data = model;
        next();
    },
    updateUserValid,
    (req, res, next) => {
        const id = {id: req.params.id};
        if(!(res.err && res.err.error)) {
            const userToUpdate = UserService.update(id, res.data);
            res.data = userToUpdate;
        }
        next();
}, responseMiddleware);

router.delete("/:id", (req, res, next) =>{
    try {
        const id = req.params.id;
        const userId = UserService.delete(id);
        res.data = userId;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},  responseMiddleware);

module.exports = router;