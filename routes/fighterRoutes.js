const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();
router.get("/", (req,res,next) =>{
    try {
        const fighters = FighterService.getAll();
        res.data = fighters;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get("/:id", (req, res, next) =>{
    try {
        const id = {
            id: req.params.id
        };
        const fighter = FighterService.getOne(id);
        res.data = fighter;
    } catch (err) {
        res.err = err;
    } finally{
        next();
    }
},  responseMiddleware);

router.post("/", createFighterValid, (req, res, next) => {
    if(!(res.err && res.err.error)) {
        FighterService.create(res.data);
    }
    next();
}, responseMiddleware);

router.put("/:id", (req, res, next) => {
        const id = {id: req.params.id};
        const dataToUpdate = req.query;
        let model = FighterService.getOne(id);
        model = Object.assign({}, model, dataToUpdate);
        res.data = model;
        next();
    },
    updateFighterValid,
    (req, res, next) => {
        const id = {id: req.params.id};
        if(!(res.err && res.err.error)) {
            const fighterToUpdate = FighterService.update(id, res.data);
            res.data = fighterToUpdate;
        }
        next();
    }, responseMiddleware);

router.delete("/:id", (req, res, next) =>{
    try {
        const id = req.params.id;
        const fighterId = FighterService.delete(id);
        res.data = fighterId;
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},  responseMiddleware);

module.exports = router;