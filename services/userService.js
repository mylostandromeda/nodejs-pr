const { UserRepository } = require('../repositories/userRepository');

class UserService {
    
    getAll() {
        const users = UserRepository.getAll();
        return users;
    }
    getOne(id) {
        const user = UserRepository.getOne(id);
        return user;
    }
    create(data) {
        const user = UserRepository.create(data);
        return user;
    }

    update(id, dataToUpdate) {
        const userUpdate = UserRepository.update(id, dataToUpdate);
        return userUpdate;
    }

    delete(id) {
        const user = UserRepository.delete(id);
        return user;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();