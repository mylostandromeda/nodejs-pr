const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

    getAll() {
        const fighters = FighterRepository.getAll();
        return fighters;
    }
    getOne(id) {
        const fighter = FighterRepository.getOne(id);
        return fighter;
    }
    create(data) {
        const fighter = FighterRepository.create(data);
        return fighter;
    }

    update(id, dataToUpdate) {
        const fighterUpdate = FighterRepository.update(id, dataToUpdate);
        return fighterUpdate;
    }

    delete(id) {
        const fighter = FighterRepository.delete(id);
        return fighter;
    }
}

module.exports = new FighterService();