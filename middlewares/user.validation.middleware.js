const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    const userModel = {};
    const userProps = req.body;

    Object.keys(user).forEach((key) => {
        userModel[key] = userProps[key];
    });

    try {
        userValidation(userModel);
        res.data = userModel;
    } catch (err) {
        res.err = {
            error: true,
            message: err
        }
    } finally {
        next();
    }
};


const updateUserValid = (req, res, next) => {
    const userModel = {};
    const userProps = res.data;

    Object.keys(user).forEach((key) => {
        userModel[key] = userProps[key];
    });

    try {
        userValidation(userModel);
        res.data = userModel;
    } catch (err) {
        res.err = {
            error: true,
            message: err
        }
    } finally {
        next();
    }
};


function userValidation(userModel){
    const mail = /^[a-z](\.?[a-z0-9])*@gmail\.com$/;
    const number = /^\+380\d{9}$/;
    if (!(userModel.firstName && userModel.firstName.length > 0)) {
        throw  "Name is required"
    }
    if (!(userModel.lastName && userModel.lastName.length > 0)) {
        throw  "Surname is required"
    }
    if (!(userModel.email && userModel.email.length > 0)) {
        throw  "Email is required"
    }
    if (!(userModel.phoneNumber && userModel.phoneNumber.length > 0)) {
        throw  "Phone is required"
    }
    if (!(userModel.password && userModel.password.length > 0)) {
        throw  "Password is required"
    }
    if(userModel.password.length < 3) {
        throw "Password must be at least 3 characters long";
    }
    if(!mail.test(userModel.email)){
        throw "Invalid email";
    }
    if(!number.test(userModel.phoneNumber)){
        throw "Invalid phone number";
    }
}
exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;