const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    const fighterModel = Object.assign({}, fighter);
    const fighterProps = req.body;

    Object.keys(fighter).forEach((key) => {
        if(fighterProps[key]) {
            fighterModel[key] = fighterProps[key];
        }
    });
    try {
        fighterValidation(fighterModel);
        res.data = fighterModel;
    } catch (err) {
        res.err = {
            error: true,
            message: err
        }
    } finally {
        next();
    }
};

const updateFighterValid = (req, res, next) => {
    const fighterModel = {};
    const fighterProps = res.data;

    Object.keys(fighter).forEach((key) => {
        fighterModel[key] = fighterProps[key];
    });

    try {
        fighterValidation(fighterModel);
        res.data = fighterModel;
    } catch (err) {
        res.err = {
            error: true,
            message: err
        }
    } finally {
        next();
    }

    next();
};


function fighterValidation(fighterModel){
    if (!(fighterModel.name && fighterModel.name.length > 0)) {
        throw  "Name is required"
    }
    if (!(typeof fighterModel.health === 'number' && fighterModel.health > 0)) {
        throw  "Health is required"
    }
    if (!(typeof fighterModel.power === 'number' && fighterModel.power> 0)) {
        throw  "Power is required"
    }
    if (!(typeof fighterModel.defense === 'number' && fighterModel.defense > 0)) {
        throw  "Defense is required"
    }
    if(!(fighterModel.defense >= 1 &&  fighterModel.defense <= 10)){
        throw "Defense should be from 1 to 10";
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;