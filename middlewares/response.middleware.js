const responseMiddleware = (req, res, next) => {
    if(req){
        if(!res.err) {
            res.send(res.data);
            next();
        } else {
            res.status(400).send(res.err);
        }
    } else {
        res.status(404).send();
    }
};

exports.responseMiddleware = responseMiddleware;